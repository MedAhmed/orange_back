from fastapi import FastAPI,Depends,Request,File, UploadFile
import pandas as pd
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
from typing import Optional
import aiofiles 
from fastapi_pagination import Page, paginate,Params
import numpy as np
import datetime as dt

class Site(BaseModel):
    sitename: str
    trouble: bool
class Cell(BaseModel):
    cell_name: str
    trouble: bool
    success_rate: float

app = FastAPI()
origins = ["*"]
global df
global df_last_day
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)



@app.get("/")
async def root():
    return 'hello'

@app.get('/sites')
async def get_sites():
    return list(set(df_last_day['eNodeB Name']))
@app.get('/cleandate')
async def get_newest_date_data():
    data[(data['Date'] >= data['Date'].max- pd.Timedelta(days=1))].to_excel('last_day_data.xlsx',index=False)
    return data[(data['Date'] <= data['Date'].max())]

@app.get('/newestsite', response_model=Page[Site])
async def get_newest_site_data(site,params: Params = Depends()):
    list_sites=[]
    if(site==''): 
        list_sites=[]
        for i in list(set(df_last_day['eNodeB Name'])):
            trouble_list=df_last_day[df_last_day['eNodeB Name']==i]['trouble']
            list_sites.append(Site(sitename=i,trouble=any(trouble_list)))
        return paginate(list_sites, params)
    else:
        for i in list(set(df_last_day[df_last_day['eNodeB Name'].str.contains(site)]['eNodeB Name'])):
            trouble_list=df_last_day[df_last_day['eNodeB Name']==i]['trouble']
            list_sites.append(Site(sitename=i,trouble=any(trouble_list)))
        return paginate(list_sites, params)    

@app.post("/files/")
async def create_file(file: Optional[bytes] = File(None)):
    df = pd.read_excel(file)
    df.Date = pd.to_datetime(df.Date)
    df_last_day = df[df['Date'] >= df['Date'].max()]
    listt=list(df_last_day['INTER-RAT HO SUCCESS RATE (LTE TO WCDMA)(%)'])
    listf=[]
    for i in listt:
        listf.append(float(str(i).replace('/','')))
    df_last_day['INTER-RAT HO SUCCESS RATE (LTE TO WCDMA)(%)']=listf
    df_last_day['success rate']=listf
    df_last_day['trouble'] = np.where(df_last_day['success rate']<80, True, False)

    return {'message':'success'}

@app.get('/{sitename}/cells/', response_model=Page[Cell])
async def get_cells_per_site(sitename,cell,params: Params = Depends()):
    df = pd.read_excel('data_cleaned_columns.xlsx')
    df_last_day= df[(df['Date'] >= dt.datetime(2022, 1, 15))]
    df['trouble'] = np.where(df['success rate']<80, True, False)
    list_cells=[]
    if(cell==''):
        for i in list(set(df.loc[df['eNodeB Name']==sitename]['Cell Name'])):
            trouble_list =df.loc[df['Cell Name']==i]['trouble']
            success_rate= df[df['Cell Name']==i]['success rate'].mean()
            list_cells.append(Cell(cell_name=i,trouble=any(trouble_list),success_rate=success_rate))
        return paginate(list_cells,params)
    else:
        for i in list(set(df.loc[df['Cell Name']==cell]['Cell Name'])):
            trouble_list =df.loc[df['Cell Name']==i]['trouble']
            list_cells.append(Cell(cell_name=i,trouble=any(trouble_list)))
            return paginate(list_cells,params)


@app.get('/{sitename}/cells/list')
async def list_cells(sitename):
    df = pd.read_excel('last_day_data.xlsx')
    return list(set(df.loc[df['eNodeB Name']==sitename]['Cell Name']))
@app.get('/{sitename}/dashboard/cells/')
async def Dashboard_cell(sitename):
    data=dict()
    df = pd.read_excel('data_cleaned_columns.xlsx')
    df=df[(df['Date'] >= dt.datetime(2022, 1, 1))]
    df['trouble'] = np.where(df['success rate']<80, True, False)

    list_nodes=list(set(df[df['eNodeB Name']==sitename]['Cell Name']))
    label=[]
    datasets=[]
    listcolors=['#4CB5F5','#B3C100','#CED2CC','#23282D','#4CB5F5','#1F3F49','#D32D41','#6AB187','#DADADA','#AC3E31']
    j=0
    for i in list_nodes:
        tmp =df[df['Cell Name']==i]['Date']
        if (len(label)<len(tmp)):
            label=list(tmp)
        datasets.append({'borderColor':listcolors[j],'fill': False,'label':i,'data':list(df[df['Cell Name']==i]['success rate']),'trouble':any(df[df['Cell Name']==i]['trouble'])})
        j=j+1
    data['labels']=label
    data['datasets']=datasets
    return data

@app.get('/{cellname}/dashboard/')
async def Dashboard_single_cell(cellname):
    df = pd.read_excel('data_cleaned_columns.xlsx')
    df=df[(df['Date'] >= dt.datetime(2022, 1, 1))]
    df['trouble'] = np.where(df['success rate']<80, True, False)
    labels=list(df.loc[df['Cell Name']==cellname]['Date'])
    data=list(df.loc[df['Cell Name']==cellname]['success rate'])
    listcolors=['#4CB5F5','#B3C100','#CED2CC','#23282D','#4CB5F5','#1F3F49','#D32D41','#6AB187','#DADADA','#AC3E31']

    datasets=[{'borderColor':listcolors[3],'label':cellname, 'data':data}]
    return {'labels':labels, 'datasets':datasets}
def to_df(file):
    data = file
    data = csv.reader(codecs.iterdecode(data,'utf-8'), delimiter='\t')
    header = data.__next__()
    df = pd.DataFrame(data, columns=header)
    return df